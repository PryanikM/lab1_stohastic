import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib.animation import FuncAnimation

class graph():
    def __init__(self, time_max, area_size_w=32, area_size_h=32):

        self.fig, self.ax = plt.subplots()

        self.__area_size_w = area_size_w
        self.__area_size_h = area_size_h
        self.__start_point = [int(area_size_w / 2), int(area_size_h / 2)]
        self.__point_counter = 0

        self.d = []

        self.area = -np.ones((self.__area_size_w, self.__area_size_h))

        self.__perimeter_points = []

        SI, SJ = np.indices((3, 3))
        SI, SJ = SI.flatten() - 1, SJ.flatten() - 1

        SI = np.delete(SI, 4)
        SJ = np.delete(SJ, 4)

        self.__offset = (SI, SJ)

        self.update_perimeters(start_point=self.__start_point)
        self.__mat = self.ax.matshow(self.area)
        self.cbar = self.fig.colorbar(self.__mat)
        self.cbar.mappable.set_clim(-2, time_max)

    def update_perimeters(self, random_point_num=None, start_point=None):

        if random_point_num is not None:
            point_x, point_y = self.__perimeter_points[random_point_num]
        else:
            point_x, point_y = start_point

        self.area[point_x, point_y] = self.__point_counter
        if len(self.__perimeter_points) != 0 and random_point_num is not None:

            self.__perimeter_points[random_point_num] = self.__perimeter_points[-1]
            self.__perimeter_points.pop(-1)


        for i in range(len(self.__offset[0])):

            if 0 <= point_x + self.__offset[0][i] < self.__area_size_w and 0 <= point_y + self.__offset[1][i] < self.__area_size_h and \
                    self.area[point_x + self.__offset[0][i]][point_y + self.__offset[1][i]] == -1:
                self.area[point_x + self.__offset[0][i]][point_y + self.__offset[1][i]] = -2

                self.__perimeter_points.append([point_x + self.__offset[0][i], point_y + self.__offset[1][i]])

    def animate(self, i):
        if len(self.__perimeter_points) - 1 != 0:
            point_ind = random.randint(0, len(self.__perimeter_points) - 1)
            self.update_perimeters(point_ind)
            self.__point_counter = i
            self.d.append(-np.log(i + 1) / np.log(1 / (area_size_w)))
            self.ax.set_title(f'Фрактальная размернось = {self.d[-1]}')
        self.__mat.set_data(self.area)
        return self.__mat


if __name__ == "__main__":

    time_max = 1000
    area_size_w = 100
    area_size_h = 100
    graph = graph(time_max, area_size_w=area_size_w, area_size_h=area_size_h)

    anim = FuncAnimation(graph.fig, graph.animate, frames=time_max, interval=1, repeat=False)

    plt.show()
    # anim.save('result/StochMod_pr1.gif',
    #                    writer='pillow',
    #                    fps=100)


    fig_d, ax_d = plt.subplots()

    ax_d.plot(np.arange(time_max + 1), graph.d)

    plt.show()
    # print(f'Фрактальная размернось = {d}')

